#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: MakeWebSite 3.8.90\n"
"PO-Revision-Date: 2015-10-29 02:58 UTC\n"
"Last-Translator: Benoît Minisini <gambas@users.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Gambas web site generator"
msgstr "Generátor webové stránky Gambasu"

#: .project:2
msgid "Generates Gambas web site from html templates and ChangeLog files."
msgstr "Gambas generuje webové stránky z HTML šablon a ChangeLog souborů."

#: MTranslation.module:9
msgid "No software patents in Europe!"
msgstr "Žádný softwarový patent v Evropě!"

#: MTranslation.module:10
msgid "The greatest formula of the world!"
msgstr "Nejlepší vzorec na světě!"

#: MTranslation.module:11
msgid "Donate with PayPal"
msgstr "Přispět s PayPal"

#: MTranslation.module:12
msgid "Flattr this"
msgstr "Flattr-tni to"

#: MTranslation.module:13
msgid "Download"
msgstr "-"

#: MTranslation.module:14
msgid "Release Notes"
msgstr "Poznámky k vydání"

#: MTranslation.module:15
msgid "Browse source code"
msgstr ""

#: MTranslation.module:16
msgid "Wiki"
msgstr ""

#: MTranslation.module:17
msgid "Bugtracker"
msgstr ""

#: MTranslation.module:18
msgid "Help"
msgstr "Nápověda"

#: MTranslation.module:19
msgid "F.A.Q."
msgstr ""

#: MTranslation.module:20
msgid "Compilation &amp; Installation"
msgstr "Kompilace &amp; Instalace"

#: MTranslation.module:21
msgid "Reporting a problem"
msgstr "Nahlášení problému"

#: MTranslation.module:22
msgid "Mailing lists/Forums"
msgstr "Mailing listy/Fóra"

#: MTranslation.module:23
msgid "Books"
msgstr "Knihy"

#: MTranslation.module:24
msgid "About"
msgstr ""

#: MTranslation.module:25
msgid "Introduction"
msgstr "Úvod"

#: MTranslation.module:26
msgid "What is Gambas?"
msgstr "Co je Gambas?"

#: MTranslation.module:27
msgid "License"
msgstr "Licence"

#: MTranslation.module:28
msgid "Screenshots"
msgstr "Náhledy"

#: MTranslation.module:29
msgid "Hall Of Fame"
msgstr "Síň slávy"

#: MTranslation.module:30
msgid "English"
msgstr "Anglicky"

#: MTranslation.module:31
msgid "French"
msgstr "Francouzky"

#: MTranslation.module:32
msgid "Spanish"
msgstr "Španělsky"

#: MTranslation.module:33
msgid "Catalonia"
msgstr "Katalánsky"

#: MTranslation.module:34
msgid "German"
msgstr "Německy"

#: MTranslation.module:35
msgid "Dutch"
msgstr ""

#: MTranslation.module:36
msgid "Czech"
msgstr ""

#: MTranslation.module:37
msgid "Turkish"
msgstr "Turecky"

#: MTranslation.module:38
msgid "Arabic"
msgstr "Arabsky"

#: MTranslation.module:39
msgid "Korean"
msgstr "Korejsky"

#: MTranslation.module:40
msgid "Chinese"
msgstr "Čínsky"

