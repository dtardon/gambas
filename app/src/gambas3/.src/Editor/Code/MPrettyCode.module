' Gambas module file

Private $cOpen As New Collection
Private $cClose As New Collection

Private $iLine As Integer
Private $iLevel As Integer
Private $bLastNewLine As Boolean
Private $bNeedNewLine As Boolean
Private $iLastDim As Integer
Private $iLastSelect As Integer
Private $bNextIndent As Boolean

Private $bIndentDim As Boolean
Private $bRemoveSpace As Boolean
Private $bKeepVoidLine As Boolean

Public Sub _init()
  
  Dim sStr As String
  
  For Each sStr In ["DO", "FOR", "ELSE", "SELECT", "CASE", "DEFAULT", "WHILE", "REPEAT", "WITH", "CATCH", "FINALLY"]
    $cOpen[sStr] = True
  Next
  For Each sStr In ["LOOP", "NEXT", "ENDIF", "END IF", "ELSE", "CASE", "DEFAULT", "WEND", "UNTIL", "END WITH", "CATCH", "FINALLY"]
    $cClose[sStr] = True
  Next
  ' IF and END SELECT are special
  
End

Private Sub RemoveLine(aPos As Integer[], iLine As Integer)

  Dim I As Integer
  
  While I < aPos.Count
    If aPos[I] = iLine Then
      aPos.Remove(I)
    Else
      If aPos[I] > iLine Then Dec aPos[I]
      Inc I
    Endif
  Wend
  
End

Private Sub InsertLine(aPos As Integer[], iLine As Integer, Optional iCount As Integer = 1)

  Dim I As Integer
  
  While I < aPos.Count
    If aPos[I] >= iLine Then aPos[I] += iCount
    Inc I
  Wend
  
End


Public Sub Run(hEditor As TextEditor, Optional iStart As Integer, Optional iEnd As Integer = -1)
  
  Dim sLine As String
  Dim aLine As String[]
  Dim iLine As Integer
  Dim iLevel As Integer
  Dim aBookmark As Integer[]
  Dim bBookmark As Boolean
  Dim aBreakpoint As Integer[]
  Dim bBreakpoint As Boolean
  Dim X As Integer
  Dim Y As Integer
  Dim aText As String[]
  Dim bModified As Boolean
  
  Inc Application.Busy
  
  aBreakpoint = hEditor.Breakpoints
  aBookmark = hEditor.Bookmarks
    
  $bIndentDim = Settings["/FormatCode/IndentLocal", 1]
  $bRemoveSpace = Settings["/FormatCode/RemoveSpaces", 0]
  $bKeepVoidLine = Settings["/FormatCode/KeepVoidLines", 0]
  
  If iEnd < 0 Then
    iEnd = hEditor.Count - 1
  Endif
  
  $iLevel = iLevel
  $bLastNewLine = True
  $iLastDim = -1
  $iLastSelect = -1
  $bNextIndent = False
  
  aText = Split(hEditor.Text, "\n")
  
  X = hEditor.Column
  Y = hEditor.Line
  
  iLine = 0
  While iLine <= iEnd
    
    $iLine = iLine
    sLine = aText[iLine]
    
    sLine = IndentLine(sLine)
    
    If iLine < iStart Then
      Inc iLine
      Continue
    Endif
    
    If $bRemoveSpace Then sLine = RTrim(sLine)
    aLine = Split(sLine, "\n")
    
    If aLine.Count <= 1 Then
      
      If aText[iLine] <> sLine Then
        aText[iLine] = sLine
        bModified = True
      Endif
      
    Else
      
      aText[iLine] = aLine[0]
      aLine.Remove(0)
      aText.Insert(aLine, iLine + 1)
      
      If aBookmark.Count Then
        InsertLine(aBookmark, iLine + 1, aLine.Max)
        bBookmark = True
      Endif
      If aBreakpoint.Count Then
        InsertLine(aBreakpoint, iLine + 1, aLine.Max)
        bBreakpoint = True
      Endif
      
      iLine += aLine.Count
      iEnd += aLine.Count
      bModified = True
      
    Endif
    
    Inc iLine
  Wend
  
  If Not $bKeepVoidLine Then
    
    iLine = Max(1, iStart)
    While iLine <= iEnd
      
      If Not Trim(aText[iLine]) And If Not Trim$(aText[iLine - 1]) Then
        
        aText.Remove(iLine - 1)
        bModified = True
        'hEditor.Remove(0, iLine - 1, 0, iLine)
        Dec iEnd
        
        If aBookmark.Count Then
          RemoveLine(aBookmark, iLine)
          bBookmark = True
        Endif
        If aBreakpoint.Count Then
          RemoveLine(aBreakpoint, iLine)
          bBookmark = True
        Endif
        
      Else
        
        Inc iLine
        
      Endif
      
    Wend
    
  Endif
  
  If bBookmark Then hEditor.Bookmarks = aBookmark
  If bBreakpoint Then hEditor.Breakpoints = aBreakpoint
  
  If bModified Then
    hEditor.Begin
    hEditor.SelectAll
    hEditor.Insert(aText.Join("\n"))
    hEditor.End
    hEditor.Goto(X, Y)
  Endif
  
  Dec Application.Busy
  
End

Private Sub IsStruct() As Boolean
  
  Dim aSym As String[] = Highlight.Symbols
  Dim iInd As Integer
  
  If aSym.Count >= 3 Then
    If aSym[iInd] = "PUBLIC" Then Inc iInd
    Return aSym[iInd] = "STRUCT"
  Endif
  
End

Private Sub IsEndStruct() As Boolean
  
  Dim aSym As String[] = Highlight.Symbols
  
  If aSym.Count = 2 And If aSym[0] = "END" And If aSym[1] = "STRUCT" Then Return True
  
End

Private Sub IndentLine(sLine As String) As String
  
  Dim aSym As String[]
  Dim sAdd As String
  Dim iNextLevel As Integer
  Dim sSym As String
  Dim sFuncSym As String
  Dim iPos As Integer
  Dim iLastType As Integer
  Dim iLevel As Integer
  Dim bNextIndent As Boolean

  sLine = LTrim(sLine)
  
  If sLine Then
    
    If Left(sLine) = "'" Then
      Return Space$(Project.TabSize * $iLevel) & sLine
    Endif
    
    aSym = Highlight.Analyze(Highlight.Purge(Trim(sLine)))
    While aSym.Count
      iLastType = Highlight.Types[aSym.Max]
      If iLastType <> Highlight.Comment And If iLastType <> Highlight.Help Then Break
      aSym.Remove(aSym.Max)
    Wend
    
    If aSym.Count Then
      
      bNextIndent = $bNextIndent
      $bNextIndent = False
      
      ' bLastString = False
      ' If $bLastString And If Highlight.Types[0] = Highlight.String Then
      '   bLastString = True
      ' Endif
      ' 
      ' $bLastString = iLastType = Highlight.String
      
      If iLastType = Highlight.Operator Then
        If InStr(":]);.!", aSym[aSym.Max]) = 0 Then 'And If aSym[0] <> "PRINT" Then 
          $bNextIndent = True
        Endif
      Else If iLastType = Highlight.String And If Highlight.Types[0] = Highlight.String Then
        bNextIndent = True
      Endif
      
      If $bNeedNewLine Then
        If Not $bLastNewLine Then sAdd = "\n"
        $bNeedNewLine = False
      Endif
      
      If aSym.Count = 2 And If aSym[1] = ":" Then
        iNextLevel = $iLevel
        Dec $iLevel
      Else If FEditor.IsCurrentProc() Then
        $iLevel = 0
        iNextLevel = 1
        $bNeedNewLine = True
        If Not $bLastNewLine Then sAdd = "\n"
      Else If FEditor.IsCurrentEndProc() Then
        $iLevel = 0
        iNextLevel = 0
        $bNeedNewLine = True
        If Not $bLastNewLine Then sAdd = "\n"
      Else If IsStruct() Then
        $iLevel = 0
        iNextLevel = 1
        If Not $bLastNewLine Then sAdd = "\n"
      Else If IsEndStruct() Then
        $iLevel = 0
        iNextLevel = 0
        $bNeedNewLine = True
      Else
        sSym = aSym[0]
        If aSym.Count > 2 Then sFuncSym = LCase(aSym[2])
        
        If sSym = "END" And If aSym.Count >= 2 Then sSym &= " " & aSym[1]
        
        If sSym = "DIM" Then
          
          $iLastDim = $iLine
          If Not $bIndentDim Then $iLevel = 0
          iNextLevel = 1
        
        Else
        
          If $iLastDim = ($iLine - 1) Then
            If Not bNextIndent Then 'Not bLastString And If Not bLastWrap Then
              If Not $bLastNewLine Then sAdd = "\n"
            Else
              Inc $iLastDim
            Endif
          Endif
        
          If sSym = "END SELECT" Then
            $iLevel -= 2
            iNextLevel = $iLevel
          Else If Right(aSym[0]) = ":" Then
            iNextLevel = $iLevel
            $iLevel = 0
          Else If sSym = "IF" Then
            iNextLevel = $iLevel
            iPos = aSym.Find("THEN")
            If iPos < 0 Or If iPos = aSym.Max Then Inc iNextLevel
          Else If sSym = "SELECT" Then
            $iLastSelect = $iLine
            iNextLevel = $iLevel + 2
          ' Else If sFuncSym = "end" Then                     'Fabien : Add End function detecetion
          '   Dec $iLevel
          '   iNextLevel = $iLevel
          '   If bLastWrap Then Inc $iLevel
          ' Else If sFuncSym = "begin" Then                   'Fabien : Add Begin function detection
          '   iNextLevel = $iLevel
          '   Inc iNextLevel
          '   If bLastWrap Then Inc $iLevel
          Else
            If $cClose.Exist(sSym) Then Dec $iLevel
            iNextLevel = $iLevel
            If $cOpen.Exist(sSym) Then Inc iNextLevel
          Endif
          
        Endif
        
      Endif
      
      $bLastNewLine = False 
      
      $iLevel = Max(0, $iLevel)
      iNextLevel = Max(0, iNextLevel)
      
      iLevel = $iLevel
      If bNextIndent Then Inc iLevel
      sLine = sAdd & Space$(Project.TabSize * iLevel) & sLine '& " '" & If(bLastWrap, "W", "") & If(bLastString, "S", "")
      $iLevel = iNextLevel
      
      Return sLine
      
    Endif
    
  Endif
  
  $bLastNewLine = True
  sLine = Space$(Project.TabSize * $iLevel)
  Return sLine
  
End
