## Package version and e-mail for bugs are defined here

m4_define([GB_VERSION], [3.10.90])
m4_define([GB_MAIL], [gambas@users.sourceforge.net])
m4_define([GB_URL], [http://gambas.sourceforge.net])

m4_define([GB_VERSION_MAJOR], [3])
m4_define([GB_VERSION_MINOR], [10])
m4_define([GB_VERSION_RELEASE], [90])
